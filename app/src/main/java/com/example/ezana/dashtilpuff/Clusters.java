package com.example.ezana.dashtilpuff;

import android.graphics.Bitmap;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by Ezana on 4/27/2015.
 */
public class Clusters {

    Random rand = new Random();
    public Bitmap objectType;
    Rect dst = new Rect();
    Rect[] dstObjects = new Rect[10];
    int onlyOnce,clusterSpot = 0;//clusterSpot when 0 means it is a top cluster
    public void setLeftRect(int newLeftRect){
        this.dst.left=newLeftRect;
    }
    public void setRightRect(int newRightRect){
        this.dst.right=newRightRect;
    }
    public void setBotRect(int newBotRect){
        this.dst.bottom=newBotRect;
    }
    public void setTopRect(int newTopRect){
        this.dst.right=newTopRect;
    }
    public void makeCluster(){
        int xDis = dst.right-dst.left;
        int yDis = dst.bottom-dst.top;
        /*
        if(xDis>yDis){
            for(int icount=0;icount<5;icount++)
                dstObjects[icount] = new Rect(dst.left+icount*xDis/5,dst.top,dst.left+(icount+1)*xDis/5,dst.bottom-yDis/2);
            for(int jcount=0;jcount<5;jcount++)
                dstObjects[jcount+5] = new Rect(dst.left+jcount*xDis/5,dst.top+yDis/2,dst.left+(jcount+1)*xDis/5,dst.bottom);
        }
        if(xDis<yDis){
            for(int kcount=0;kcount<5;kcount++)
                dstObjects[kcount] = new Rect(dst.left,dst.top+kcount/5,dst.right-xDis/2,dst.top+(kcount+1)/5);
            for(int lcount=0;lcount<5;lcount++)
                dstObjects[lcount+5] = new Rect(dst.left+xDis/2,dst.top+lcount/5,dst.right,dst.top+(lcount+1)/5);
        }
        */
        if(onlyOnce==0) {
            for (int icount = 0; icount < 10; icount++) {
                int randomNumX = rand.nextInt((dst.right - dst.left) + 1) + dst.left;
                int randomNumY = rand.nextInt(((dst.bottom-64) - (dst.top+64)) + 1) + (dst.top+64);
                dstObjects[icount] = new Rect(randomNumX - 64, randomNumY - 64, randomNumX + 64, randomNumY + 64);
            }

            onlyOnce++;
        }
        else{
            for(int jcount=0;jcount<10;jcount++){
                dstObjects[jcount].left-=DashTillPuffSurfaceView.velocity;
                dstObjects[jcount].right-=DashTillPuffSurfaceView.velocity;
            }
        }
    }
}

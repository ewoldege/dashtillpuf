package com.example.ezana.dashtilpuff;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Ezana on 4/19/2015.
 */
public class Trajectory implements TimeConscious {
    CosmicFactory cosmicFactory = new CosmicFactory();
    static int ticks = 0;
    static int disX = DashTillPuffSurfaceView.velocity * 20;
    private static ArrayList<Integer> points = new ArrayList<Integer>();
    private static final String TAG = "Specific Event Handling Messages";
    int prevRandom = 0;
    static int randomNum, randomNumtemp;
    Random rand = new Random();

    @Override
    public void tick(Canvas canvas) {
        int cheight = canvas.getHeight();
        if (ticks != 0) {
            for (int j = 0; j < points.size() / 2; j++) {
                points.set(2 * j, points.get(2 * j) - DashTillPuffSurfaceView.velocity);
            }
        }

        if (ticks == 0) {
            points.add(0);
            points.add(MainActivity.height - 128);
            for (int i = 0; i < 9; i++) {
                randomNumtemp = rand.nextInt(cheight - 300) + 150;
                //randomNum=(int)((prevRandom+0.25*randomNumtemp)/2);
                randomNum = randomNumtemp;
                points.add(disX * (i + 1));
                points.add(randomNum);
                prevRandom = randomNumtemp;

            }
        } else if (ticks % 20 == 0) {
            points.remove(0);
            points.remove(0);
            randomNumtemp = rand.nextInt(cheight - 300) + 150;
            //randomNum=(int)((prevRandom+0.25*randomNumtemp)/2);

            randomNum = randomNumtemp;
            points.add(points.get(points.size() - 2) + disX);
            points.add(randomNum);
            prevRandom = randomNumtemp;

        }
        cosmicFactory.getPoints(points);
        cosmicFactory.tick(canvas);
        this.draw(canvas);
        ticks++;
    }

    public void draw(Canvas c) {

        Path path = new Path();
        path.moveTo((points.get(0)), (points.get(1))); // Move to first point
        for (int i = 2; i < points.size(); i += 2) {
            path.lineTo((points.get(i)), (points.get(i + 1)));
        }
        //path.offset(-DashTillPuffSurfaceView.velocity*ticks,0);
        Paint paint = new Paint();
        paint.setAlpha(255);
        paint.setColor(Color.CYAN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(8);

        paint.setPathEffect(new DashPathEffect(new float[]{5, 10}, 0));
// Set paint color , alpha , line width , dashed style , etc .
        c.drawPath(path, paint);

    }


}



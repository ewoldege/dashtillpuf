package com.example.ezana.dashtilpuff;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Ezana on 4/19/2015.
 */
public class CosmicFactory implements TimeConscious {
    private static final String TAG = "Specific Event Handling Messages";
    public ArrayList < Clusters > chosenOnes=new ArrayList<Clusters>();
    public ArrayList points;
    Bitmap chosen;
    static int bigger,smaller,fcount,x1,x2,x3,y1,y2,y3,xtemp,ytemp,randomNum=0;

    @Override
    public void tick ( Canvas canvas ) {
        int cheight = canvas.getHeight();
        if(Trajectory.ticks%40==0) {


            Random rand = new Random();
            randomNum = rand.nextInt(10);
            switch (randomNum) {
                case 0:
                    chosen = MainActivity.bitmap_blackhole;
                    break;
                case 1:
                    chosen = MainActivity.bitmap_blueplanet;
                    break;
                case 2:
                    chosen = MainActivity.bitmap_cloud;
                    break;
                case 3:
                    chosen = MainActivity.bitmap_earth;
                    break;
                case 4:
                    chosen = MainActivity.bitmap_glossyplanet;
                    break;
                case 5:
                    chosen = MainActivity.bitmap_goldenstar;
                    break;
                case 6:
                    chosen = MainActivity.bitmap_neutronstar;
                    break;
                case 7:
                    chosen = MainActivity.bitmap_shinystar;
                    break;
                case 8:
                    chosen = MainActivity.bitmap_star;
                    break;
                case 9:
                    chosen = MainActivity.bitmap_startwo;
                    break;
            }

            x1=(int)points.get(points.size()-6);
            x2=(int)points.get(points.size()-4);
            x3=(int)points.get(points.size()-2);
            y1=(int)points.get(points.size()-5);
            y2=(int)points.get(points.size()-3);
            y3=(int)points.get(points.size()-1);
            xtemp = (x1 + x2) / 2;
            ytemp = (y1 + y2) / 2;
            Clusters clusters = new Clusters();
            clusters.objectType = chosen;
            if(y2>y1) {
                bigger = y2;
                smaller=y1;
            }
            else {
                bigger = y1;
                smaller=y2;
            }

            if((ytemp)>cheight/2) {
                clusters.dst = new Rect(x1, 0, x2, smaller);
                clusters.clusterSpot=0;
                chosenOnes.add(clusters);
                fcount++;
            }
            else{
                clusters.dst = new Rect(x1, bigger, x2, cheight);
                clusters.clusterSpot=1;
                chosenOnes.add(clusters);
                fcount++;
            }

        }
        for(int b=0;b<chosenOnes.size();b++)
            chosenOnes.get(b).makeCluster();
        drawClusters(canvas);
        for(int i=0;i<chosenOnes.size();i++){
            chosenOnes.get(i).setLeftRect(chosenOnes.get(i).dst.left-DashTillPuffSurfaceView.velocity);
            chosenOnes.get(i).setRightRect(chosenOnes.get(i).dst.right-DashTillPuffSurfaceView.velocity);
            if(chosenOnes.get(i).dst.right<=0){
                chosenOnes.remove(i);
                i-=1;
            }
        }
        //Log.i(TAG,"Size of Objects"+chosenOnes.size());
        //Log.i(TAG,"Size of Lines"+points.size());


// Create new ‘‘ clusters ’’ of cosmic objects . Alternately place
// clusters of cosmic objects above and below the guiding
// trajectory .

// Randomly select the type of cluster objects from a list
// ( e . g . , stars , clouds , planets , etc .) . So all objects in
// a cluster are of the same type .

// Remove cosmic objects ( stars , planets , etc .) that moved out
// of the scene .

    }
    public void drawClusters(Canvas canvas){

        Paint paint = new Paint() ;
        paint . setAlpha ( 255 ) ; // Control transparency

        for(int i=0;i<chosenOnes.size();i++) {
            for(int j=0;j<10;j++)
                canvas.drawBitmap(chosenOnes.get(i).objectType, null, chosenOnes.get(i).dstObjects[j], paint);
        }
    }
    public void getPoints(ArrayList point){
        points = point;
    }
}
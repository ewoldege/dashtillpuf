package com.example.ezana.dashtilpuff;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;


public class MainActivity extends ActionBarActivity {
    public static int width;
    public static int height;
    public static Bitmap bitmap_blueplanet,bitmap_blackhole,bitmap_cloud,bitmap_earth,bitmap_glossyplanet,bitmap_goldenstar,bitmap_neutronstar,bitmap_shinystar,
            bitmap_spaceship,bitmap_star,bitmap_startwo,bitmap_wallpaper;
    public Activity activity = this;
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        super . onCreate ( savedInstanceState ) ;

        BitmapFactory. Options options = new BitmapFactory . Options () ;
        bitmap_blackhole = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_black_hole, options) ;
        bitmap_blueplanet = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_blue_planet, options) ;
        bitmap_cloud = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_cloud, options) ;
        bitmap_earth = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_earth, options) ;
        bitmap_glossyplanet = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_glossy_planet, options) ;
        bitmap_goldenstar = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_golden_star, options) ;
        bitmap_neutronstar = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_neutron_star, options) ;
        bitmap_shinystar = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_shiny_star, options) ;
        bitmap_spaceship = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_space_ship, options) ;
        bitmap_star = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_star, options) ;
        bitmap_startwo = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_star_two, options) ;
        bitmap_wallpaper = BitmapFactory. decodeResource(getResources(),
                R.drawable.dash_till_puff_wallpaper, options) ;



        //new Layout Type
        RelativeLayout WelcomeScreen = new RelativeLayout(this);
        WelcomeScreen.setBackgroundColor(Color.CYAN);

        //Create some buttons for difficulty level


        Button medium = new Button(this);
        medium.setText("START");
        medium.setBackgroundColor(Color.GREEN);




        //Giving IDs to the buttons

        medium.setId(2);


        //Grabbing the dimensions


        RelativeLayout.LayoutParams mediumButton = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT

        );


        //Setting the placement rules. Easy button will be aligned in the center, but will be above medium
        //Hard will also be in the center but will be below medium
        //They both also have some space in between the buttons; hence, the need for margins.


        mediumButton.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mediumButton.addRule(RelativeLayout.CENTER_VERTICAL);



        //I used this tool in order to make this display the same across any device.
        Resources res = getResources();
        int pixelsW = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200,
                res.getDisplayMetrics());
        int pixelsH = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,75,
                res.getDisplayMetrics());
        int pixelradius = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25,
                res.getDisplayMetrics());



        medium.setWidth(pixelsW);


        medium.setHeight(pixelsH);


        //Added buttons along with their accompanying design rules to the main view.

        WelcomeScreen.addView(medium,mediumButton);


        //Set this view as the main view for this activity.
        setContentView(WelcomeScreen);

        //EVENT HANDLING

        medium.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v){


                        Intent i = new Intent(MainActivity.this, GameActivity.class);
                        startActivity(i);
                        //setContentView( new GameView( getBaseContext() ) );
                    }
                }
        );
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void restartActivity(){
        Intent intent=new Intent();
        intent.setClass(this, this.getClass());
        this.startActivity(intent);
        this.finish();
    }
}

package com.example.ezana.dashtilpuff;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.graphics.Canvas;
import android.widget.Space;


public class DashTillPuffSurfaceView extends SurfaceView implements SurfaceHolder. Callback , TimeConscious {
    private static final String TAG = "Specific Event Handling Messages";
    public static double velocity1=0.0125*MainActivity.width;
    public static int velocity=(int)velocity1;
    public int x1=0;
    public int x2=MainActivity.width;
    public int y1=0;
    public int y2=MainActivity.height;
    public int x3=x2+MainActivity.width;
    public int SpaceBot = 850;
    public int SpaceTop = SpaceBot-128;
    public int SpaceVelocity=20;
    public final int bottomScreen = SpaceBot;
    public int collisionFlag=0;
    public boolean rise;
    public boolean fall;
    int ticks,score=0;
    MediaPlayer mySong = MediaPlayer.create(getContext(),R.drawable.bitmusic);
    SpaceShip spaceShip = new SpaceShip();
    MainActivity main = new MainActivity();




    Rect dst = new Rect(x1, y1, x2, y2); // Where to draw .
    Rect dst2 = new Rect(x2, y1, x3, y2);


    Trajectory trajectory = new Trajectory();
    CosmicFactory cosmicFactory = new CosmicFactory();
    DashTillPuffRenderThread renderThread;

    public DashTillPuffSurfaceView ( Context context ) {
        super ( context ) ;
        getHolder () . addCallback ( this ) ;
    }
    @Override
    public void surfaceCreated ( SurfaceHolder holder ) {
        renderThread = new DashTillPuffRenderThread ( this ) ;
        renderThread . start () ;





// Create the sliding background , cosmic factory , trajectory
// and the space ship

    }
    @Override
    public void surfaceChanged ( SurfaceHolder holder ,
                                 int format , int width , int height ) {
// Respond to surface changes , e . g . , aspect ratio changes .
    }

    @Override
    public void surfaceDestroyed ( SurfaceHolder holder ) {
// The cleanest way to stop a thread is by interrupting it .
// BubbleShooterThread regularly checks its interrupt flag .
        renderThread . interrupt () ;
    }
    @Override
    public boolean onTouchEvent ( MotionEvent e ) {
        switch ( e . getAction () ) {
            case MotionEvent . ACTION_DOWN :
                rise = true;
                fall = false;
                // Thrust the space ship up .
                break ;
            case MotionEvent . ACTION_UP :
                rise = false;
                fall = true;
                // Let space ship fall freely .
                break ;
        }
        return true ;
    }
    @Override
    public void onDraw ( Canvas c ) {
        super . onDraw ( c ) ;
// Draw everything ( restricted to the displayed rectangle ) .
    }
    @Override
    public void tick ( Canvas c ) {
        mySong.start();

            if (rise) {
                SpaceBot -= SpaceVelocity;
                SpaceTop -= SpaceVelocity;

            } else if (fall) {
                SpaceBot += SpaceVelocity;
                SpaceTop += SpaceVelocity;

            }

            if (SpaceTop < 0) {
                SpaceTop = 0;
                SpaceBot = SpaceTop + 128;
            }
            if (SpaceBot > bottomScreen) {
                SpaceBot = bottomScreen;
                SpaceTop = SpaceBot - 128;
            }
            Paint paint = new Paint();
            paint.setAlpha(255); // Control transparency


            c.drawBitmap(MainActivity.bitmap_wallpaper, null, dst, paint);
            spaceShip.makeRectangle(SpaceTop, SpaceBot);
//        Rect dstD = new Rect(660-128, 561-128, 660+128, 561+128);
            trajectory.tick(c);
            cosmicFactory.tick(c);
            c.drawBitmap(MainActivity.bitmap_spaceship, null, spaceShip.dstS, paint);
//        c.drawBitmap(MainActivity.bitmap_blackhole,null,dstD,paint);


            if (trajectory.cosmicFactory.chosenOnes.size() != 0) {
                collisionFlag = collisionDetection(c);
            }
            Paint paint2 = new Paint() ;
            paint2 . setAlpha(255) ; // Control transparency
            paint2.setColor(Color.WHITE);
            paint2.setTextSize(40);
            paint2.setTypeface(Typeface.DEFAULT_BOLD);

            //textPaint.setTextSize(40);
            if (ticks % 20 == 0)
                score += 10;
            //c.drawText("Score: " + score, (float) (c.getWidth() * 0.8), (float) (c.getHeight() * 0.85), textPaint);
            c.drawText("Score: " + score, MainActivity.width - 250, 40, paint2);



        ticks++;

// Tick background , space ship , cosmic factory , and trajectory .
// Draw everything ( restricted to the displayed rectangle ) .
    }

    public int collisionDetection(Canvas canv){

        int output=0;

        for(int b=0;b<trajectory.cosmicFactory.chosenOnes.size();b++){
            for(int c=0;c<10;c++){
                if((Math.abs(spaceShip.dstS.left-trajectory.cosmicFactory.chosenOnes.get(b).dstObjects[c].left)<128)&&(Math.abs(spaceShip.dstS.top-trajectory.cosmicFactory.chosenOnes.get(b).dstObjects[c].top)<128)){
                    output=1;
                    velocity=0;
                    SpaceVelocity=0;
                    score=0;


                    SystemClock.sleep(3000);
                    SpaceBot = canv.getHeight();
                    SpaceTop = SpaceBot-128;
                    trajectory = new Trajectory();
                    cosmicFactory = new CosmicFactory();
                    velocity = (int) velocity1;
                    SpaceVelocity=20;
                    //((Activity)this.getContext()).finish();
                    //DashTillPuffSurfaceView view = new DashTillPuffSurfaceView(getContext());
                    //Canvas canv = new Canvas();
                    //view.tick(canv);


                }/*
                else if((Math.abs(spaceShip.dstS.right-trajectory.cosmicFactory.chosenOnes.get(c).dst.right)<128)&&(Math.abs(spaceShip.dstS.top-trajectory.cosmicFactory.chosenOnes.get(c).dst.top)<128)){
                    output=1;
                    Log.i(TAG,"COLLIDED!");
                    velocity=0;
                }*/
            }
        }
        return output;
    }
}



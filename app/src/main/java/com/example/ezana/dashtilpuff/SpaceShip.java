package com.example.ezana.dashtilpuff;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by Ezana on 5/1/2015.
 */
public class SpaceShip {
    int xLocation=0;
    int yLocation=0;
    Bitmap spaceship = MainActivity.bitmap_spaceship;
    Rect dstS;

    public void makeRectangle(int SpaceTop,int SpaceBot) {
        dstS= new Rect(0, SpaceTop, 128, SpaceBot);
        xLocation = (dstS.left+dstS.right)/2;
        yLocation = (dstS.top+dstS.bottom)/2;
    }
}

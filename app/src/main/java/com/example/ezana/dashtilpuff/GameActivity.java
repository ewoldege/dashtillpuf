package com.example.ezana.dashtilpuff;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by Ezana on 4/17/2015.
 */
//CREATED A NEW ACTIVITY FILE IN ORDER TO HOST THE ACTUAL GAME ACTIVITY
public class GameActivity extends ActionBarActivity {
    public static int width;
    public static int height;
    public static int diffFlag=1;
    private static final String TAG = "Specific Event Handling Messages";
    public GameActivity game = this;
    public static DashTillPuffSurfaceView view;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        DashTillPuffSurfaceView view = new DashTillPuffSurfaceView(getBaseContext());
        setContentView ( view ) ;


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void gameListener(int flag){
        if(flag==1)
            game.finish();
    }
}


